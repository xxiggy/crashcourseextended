-----------------------------------------------------------------------------------------------
-- Client Lua Script for CrashCourseExtended
-- CrashCourse released by Alex Martin under MIT license. As of 19 June 2014 listed on Curse with Project Manager __kiiri.
-- CrashCourseExtended (new version) by xxiggy are also released under MIT license. 
-----------------------------------------------------------------------------------------------
--[[
CHANGE LOG
-  13 July 2014  -
Created separate variable rover_debug to control portions of the debug code that rely on Rover addon. 
Setting debug to true without Rover no longer causes an error.
Added target level detection to support veteran mode implementation. 
Known issue: Since all level 50s are considered veteran, normal mode SSM is getting veteran guides. 
     xxiggy
- 16 July 2014 - 
Created separate addon for raids, CrashCourseRaid. 
     xxiggy
]]--
-----------------------------------------------------------------------------------------------
require "Window"
require "GroupLib"
 
-----------------------------------------------------------------------------------------------
-- CrashCourseExtended Module Definition
-----------------------------------------------------------------------------------------------
local CrashCourseExtended = {}
 
-----------------------------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------
-- "Globals" (within the addon context)
-----------------------------------------------------------------------------------------------

local diCrashCourses = {} -- a dictionary of {"mob_name":["Message 1", "Message 2"...], ...}
local arSilencedMobs = {} -- an array of mobs that we have already said "crash course available for mob" once this session.
local strCurrentTarget = ""
local nCurrentTargetLevel = 0
local strSlashCommand = "ccx"
local debug = false
local rover_debug = false

 
-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function CrashCourseExtended:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self 
    return o
end

function CrashCourseExtended:Init()
	local bHasConfigureFunction = false
	local strConfigureButtonText = ""
	local tDependencies = {}
    Apollo.RegisterAddon(self, bHasConfigureFunction, strConfigureButtonText, tDependencies)
end
 

-----------------------------------------------------------------------------------------------
-- CrashCourseExtended OnLoad
-----------------------------------------------------------------------------------------------
function CrashCourseExtended:OnLoad()
	Apollo.RegisterSlashCommand(strSlashCommand, "OnCrashCourseExtendedOn", self)
	--Apollo.RegisterSlashCommand("getzone", "OnCrashGetZone", self)
	--Apollo.RegisterEventHandler("VarChange_ZoneName", "OnChangeZoneName", self)
	Apollo.RegisterEventHandler("TargetUnitChanged", "OnTargetUnitChanged", self)
	
	-- init rover
	self:PublishVarsToRover()
	
	-- load mobs
	self:LoadMobs()
end

function CrashCourseExtended:PublishVarsToRover()
	if rover_debug then
		SendVarToRover("CC: diCrashCourses", diCrashCourses)
		SendVarToRover("CC: arSupportedZones", arSupportedZones)
		SendVarToRover("CC: arSilencedMobs", arSilencedMobs)
		SendVarToRover("CC: strCurrentTarget", strCurrentTarget)
		SendVarToRover("CC: nCurrentTargetLevel", nCurrentTargetLevel)
		SendVarToRover("CC: bCurrentZoneSupported", bCurrentZoneSupported)
		SendVarToRover("CC: strCurrentZone", strCurrentZone)
	end
end

-----------------------------------------------------------------------------------------------
-- CrashCourseExtended Event Handlers
-----------------------------------------------------------------------------------------------

function CrashCourseExtended:OnTargetUnitChanged(unitTarget)
	if unitTarget == nil then
		strCurrentTarget = ""
		nCurrentTargetLevel = 0
		return
	end
	
	strCurrentTarget = unitTarget:GetName()
	nCurrentTargetLevel = unitTarget:GetLevel()
	if nCurrentTargetLevel == 50 then
		strCurrentTarget = "Vet " .. strCurrentTarget
	end
	if self:MobIsSupported(strCurrentTarget) == true and self:MobOutputIsSilenced(strCurrentTarget) == false then
		table.insert(arSilencedMobs, self:JSONifyString(strCurrentTarget))
		self:WriteLog("Type '/" .. strSlashCommand .. "' to output target information")
	end
	
	self:PublishVarsToRover()
end

-----------------------------------------------------------------------------------------------
-- CrashCourseExtended Functions
-----------------------------------------------------------------------------------------------

-- on SlashCommand "/cc"
function CrashCourseExtended:OnCrashCourseExtendedOn(cmd, args)
	local strTargetJSON = ""
		
	-- get the mob that we need to output
	if args == "" then
		self:WriteDebug("Showing crash course for target")
		if unitTarget ~= nil then
			nCurrentTargetLevel = unitTarget:GetLevel ()
			if nCurrentTargetLevel == 50 then
				strCurrentTarget = "Vet " .. strCurrentTarget
				self:WriteDebug("(Vet mode)")
			end
		end
	else 		
		self:WriteDebug("Showing crash course for mob: " .. args)
		strCurrentTarget = args
	end
	
	-- get the messages to print
	strTargetJSON = self:JSONifyString(strCurrentTarget)
	self:WriteDebug("Searching for mob: " .. strTargetJSON)
	local arMobMessages = diCrashCourses[strTargetJSON]
	
	-- print the messages
	if arMobMessages ~= nil then
		self:OutputToGroup("-- Strategy for " .. strCurrentTarget .. " --")
		for _,msg in pairs(arMobMessages) do
			self:OutputToGroup(msg)
		end
	else
		self:WriteLog("No crash course for this mob")
	end
	
	self:PublishVarsToRover()
end

function CrashCourseExtended:MobOutputIsSilenced(strMobName)
	local strMobJSON = self:JSONifyString(strMobName)
	
	-- look through the silenced mobs array
	for _,val in pairs(arSilencedMobs) do	
		-- if the mob is there, return true
 		if val == strMobJSON then
    		return true
    	end
 	end

	-- if we get here, return false
	return false
end

function CrashCourseExtended:OutputToGroup(strText)
	if GroupLib.InInstance() == true then
		ChatSystemLib.Command(string.format("/i %s", strText))
	elseif GroupLib.InGroup() == true then
		ChatSystemLib.Command(string.format("/p %s", strText))
	else
		self:WriteLog(strText)
	end
end

function CrashCourseExtended:WriteLog(strText)
	ChatSystemLib.PostOnChannel(ChatSystemLib.ChatChannel_System, strText, "CrashCourseExtended")
end

function CrashCourseExtended:WriteDebug(strText)
	if debug then
		ChatSystemLib.PostOnChannel(ChatSystemLib.ChatChannel_System, strText, "CC Debug")
	end
end

function CrashCourseExtended:MobIsSupported(strMobName)
	local strMobJSON = self:JSONifyString(strMobName)
	
	-- look through the mobs dictionary
	for key,val in pairs(diCrashCourses) do
	
		-- if the zone is in the array, return true
 		if key == strMobJSON then
    		return true
    	end
 	end

	-- if we get here, return false
	return false
end

function CrashCourseExtended:JSONifyString(strOriginal)
	-- equivalent to var edited = str.toLowerCase().replace(/[\s]/g, '_').replace(/[^_a-z0-9]/gi, '');
	local strNew = strOriginal:lower()
	strNew = strNew:gsub("%s", "_")
	strNew = strNew:gsub("[^_a-zA-Z0-9]", "")
	return strNew
end

function CrashCourseExtended:SpellDesc(strName, bInterrupt, iIntArmor, bOptional)
	-- defaults
	iIntArmor = iIntArmor or 0
	bOptional = bOptional or false
	bInterrupt = bInterrupt or false
	
	-- build the string
	local strDesc = strName
	local strInterrupt = "**Interrupt** "
	local strDodge = "Dodge out of "
	local strIntArmor = "(" .. iIntArmor .. " IA)"
	local strOptional = "(Optional)"
	
	if iIntArmor > 0 then
		strDesc = strDesc .. " " .. strIntArmor
	end
	
	if bOptional == true then
		strDesc = strDesc .. " " .. strOptional
	end
	
	if bInterrupt then
		strDesc = strInterrupt .. strDesc
	else
		strDesc = strDodge .. strDesc
	end
	
	return strDesc
end

-----------------------------------------------------------------------------------------------
-- Initialize the "supported mobs" array (it's down here because it's long)
-----------------------------------------------------------------------------------------------

function CrashCourseExtended:LoadMobs()
	local mobs = {}
	
	--------------------------------------------------------------------------------------------------------------------------------
	-- Kel Voreth --	
	mobs["blood_pit_necromancer"] = {
		self:SpellDesc("Wail of the Darkwitch", true, 1),
		self:SpellDesc("Raise Dead", true, 1, true)
	}
	mobs["vet_blood_pit_necromancer"] = {
		self:SpellDesc("Wail of the Darkwitch", true, 3),
		self:SpellDesc("Raise Dead", true, 3, true)
	}
	mobs["grond_the_corpsemaker"] = {
		"Stack on one side of Grond (avoid front and rear cleave)",
		self:SpellDesc("Thrash", true, 2),
		self:SpellDesc("Bellow") .. " (Optional)",
		self:SpellDesc("Mutilate", false, 0, true),
		"AOE adds or kite into bone traps"
	}
	mobs["vet_grond_the_corpsemaker"] = {
		"Stay to Grond's sides (avoid front and rear cleave)",
		self:SpellDesc("Thrash", true, 2),
		"Bellow disarms. Dodge it or use CC break. (Optional)",
		"Kite adds into bone traps quickly"
	}
	mobs["enslaved_protector"] = {
		self:SpellDesc("Exterminate", true, 1),
		"DPS can ignore adds"
	}
		mobs["vet_enslaved_protector"] = {
		self:SpellDesc("Exterminate", true, 4),
		"Cleave adds"
	}
	mobs["enslaved_augmentor"] = {
		self:SpellDesc("Rejuvinate", true, 1)
	}
	mobs["vet_enslaved_augmentor"] = {
		self:SpellDesc("Rejuvinate", true, 2)
	}
	mobs["voreth_mechano_slaver"] = {
		"Avoid all attacks"
	}
	mobs["vet_voreth_mechano_slaver"] = {
		"Avoid all attacks"
	}
	mobs["voreth_necromancer"] = {
		self:SpellDesc("Wail of the Darkwitch", true, 1),
		self:SpellDesc("Raise Dead", true, 1, true)
	}
	mobs["vet_voreth_necromancer"] = {
		self:SpellDesc("Wail of the Darkwitch", true, 3),
		self:SpellDesc("Raise Dead", true, 3, true)
	}
	mobs["darkwitch_gurka"] = {
		"Spread around Gurka",
		self:SpellDesc("Deadly Defilement") .. " Drops 5 circles.",
		self:SpellDesc("Afflicted Soul"),
		self:SpellDesc("Binding Dark", true, 2)
	}
	mobs["vet_darkwitch_gurka"] = {
		"Spread around Gurka",
		self:SpellDesc("Deadly Defilement") .. " Drops 5 circles.",
		self:SpellDesc("Afflicted Soul"),
		self:SpellDesc("Binding Dark", true, 4)
	}
	mobs["slavemaster_drokk"] = {
		self:SpellDesc("Suppression Wave", true, 2, true) .. " If not interrupted, avoid.",
		"First time Drokk disappears, kill last Bombshell Construct that spawns near green door - avoid the rest",
		"When Enslaved, kill your tether or use CC break, then assist your healer -> tank -> other DPS",
		"Destructo Constructs target should stay in the back of the room, then run away if bots get close. If not targeted, help CC and kill constructs.",
		"Below 20%, Drokk will summon Bombshell Constructs while fighting. Move the boss out of the blast radius"
	}
	mobs["vet_slavemaster_drokk"] = {
		"Bring: snare/root/stun, CC break, interrupts.",
		"Drokk will summon Bombshell Constructs while fighting. Move the boss out of the blast radius.",
		"First time Drokk disappears, kill last Bombshell Construct that spawns near green door - avoid the rest",
		"When Enslaved, kill your tether or use CC break, then assist your healer -> tank -> other DPS",
		self:SpellDesc("Suppression Wave", true, 2) .. " If not interrupted, avoid. Spawns gas pool in melee range. Kite boss out of pools.",
		"Whenever someone is tethered, everyone kill tether quickly.",
		"Destructo Constructs' target should stay in the back of the room, then run away if bots get close. Heavy heals on target. If not targeted, help CC and kill constructs.",
	}
	mobs["voreth_beastmaster"] = {
		"Ignore the dogs",
		self:SpellDesc("Pulverize", true, 2)
	}
	mobs["vet_voreth_beastmaster"] = {
		self:SpellDesc("Pulverize", true, 2),
		"Stun dogs after Beastmaster dies or if damage too high."
	}
	mobs["voreth_blinded_slaver"] = {
		"Cleave adds",
		"Stand behind to avoid telegraphs"
	}
	mobs["vet_voreth_blinded_slaver"] = {
		self:SpellDesc("Conflagrate", true, 2),
		"Interrupt or dodge Magma Plume",
		"Interrupt Motivation (Optional)",
		"Cleave adds."
	}
	mobs["voreth_darkwitch"] = {
		self:SpellDesc("Rupture", true, 2),
		self:SpellDesc("Impending Doom", true, 2, true),
		"Stack for healing and dispels",
		"Dispel the DoT - it hits hard"
	}
	mobs["vet_voreth_darkwitch"] = {
		self:SpellDesc("Rupture", true, 3),
		self:SpellDesc("Impending Doom", true, 3, true),
		"Stack for healing and dispels",
		"Dispel the DoT - it hits hard"
	}
	mobs["voreth_battlesworn"] = {
		self:SpellDesc("Hammer of Wrath", true, 2),
		self:SpellDesc("Seismic Trample", true, 2, true)
	}
	mobs["vet_voreth_battlesworn"] = {
		self:SpellDesc("Hammer of Wrath", true, 3),
		self:SpellDesc("Seismic Trample", true, 3, true)
	}
	mobs["forgemaster_trogun"] = {
		self:SpellDesc("Volcanic Strike", true, 2, true),
		"Tank near center of the room, or nearer the entrance",
		"Group behind him or on a side, for healing",
		"Intercept balls of fire before they get to boss. Before starting, plan which sides people will get.",
		"When Trogun moves to forge for Exanite Weapon, stand on the entrance side of the room and dodge the projectiles radiating from him",
		"After Exanite Weapon, Forgemaster's Call will also shoot projectiles out from Trogun. Dodge these while still intercepting the balls of fire"
	}	
	mobs["forgemaster_trogun"] = {
		self:SpellDesc("Volcanic Strike", true, 4, true),
		"Tank near center of the room, or nearer the entrance",
		"Group behind him or on a side, for healing",
		"Intercept balls of fire before they get to boss. Before starting, plan which sides people will get.",
		"When Trogun moves to forge for Exanite Weapon, stand on the entrance side of the room and dodge the projectiles radiating from him",
		"After Exanite Weapon, Forgemaster's Call will also shoot projectiles out from Trogun. Dodge these while still intercepting the balls of fire"
	}	
	
	--------------------------------------------------------------------------------------------------------------------------------
	-- Stormtalon's Lair --
	mobs["thundercall_sentinel"] = {
		self:SpellDesc("Shockwave", true)
	}	
	mobs["vet_thundercall_sentinel"] = {
		self:SpellDesc("Shockwave", true, 1, true)
	}
	mobs["thundercall_shaman"] = {
		self:SpellDesc("Rejuvinate", true, 1)
	}

	mobs["vet_thundercall_shaman"] = {
		self:SpellDesc("Rejuvinate", true, 2)
	}
	mobs["thundercall_stormwatcher"] = {
		self:SpellDesc("Rampage", true, 1),
		self:SpellDesc("Hack and Slash"),
		"Cleave birds"
	}
	mobs["vet_thundercall_stormwatcher"] = {
		"Interrupt as often as possible",
		self:SpellDesc("Rampage", true, 1),
		self:SpellDesc("Hack and Slash", true, 1),
		"Cleave birds"
	}
	mobs["thundercall_stormweaver"] = {
		"Stacking debuff MUST be removed - stack for dispels",
		self:SpellDesc("Conduction", true, 1) .. " - very high damage!"
	}
	mobs["vet_thundercall_stormweaver"] = {
		"Stacking debuff MUST be removed - stack for dispels",
		self:SpellDesc("Conduction", true, 2) .. " - very high damage!"
	}
	mobs["bladewind_the_invoker"] = {
		self:SpellDesc("Thunder Cross", true, 2, true),
		"At 60% the boss disarms the group and becomes immune. Get weapon and kill the Thundercall Channelers one at a time in a clockwise direction.",
		"While killing Channelers, drop Lightning Strike telegraphs on the one you are attacking",
		"Once all four channelers are dead, the boss disarms the group again. Tank must taunt right before disarm or ASAP afterward",
		"Avoid Static Wisps (they stun)."
	}
	mobs["vet_bladewind_the_invoker"] = {
		"Interrupt as many casts as possible.",
		self:SpellDesc("Thunder Cross", true, 2, true),
		self:SpellDesc("Electrostatic Pulse", true, 2, true) .. " - casts 3 times in a row",
		"Phase 2 - boss disarms group, goes immune. Get weapon and kill the Thundercall Channelers one at a time in a clockwise direction.",
		"Always drop Lightning Strike telegraphs on the Thundercall Channeler you are attacking",
		"Once all four channelers are dead, the boss disarms the group again. Tank must taunt right before disarm or ASAP afterward",
		"Avoid Static Wisps (they stun)."
	}
	mobs["overseer_driftcatcher"] = {
		self:SpellDesc("Gust Convergence", true, 2) .. " If adds do come, stun and kill quickly."		
	}
	mobs["vet_overseer_driftcatcher"] = {
		"Stacking debuff MUST be removed - stack for dispels",
		self:SpellDesc("Gust Convergence", true, 2) .. " If adds do come, cleave them down.",		
		"DPS down the absorb shield as soon as possible.",
		self:SpellDesc("Lightning Storm", true, 2) .. " Spread out slightly to dodge lightning. Interrupt immediately after shield is down.",
	}
	mobs["skyborn_tempest"] = {
		self:SpellDesc("Manifest Cyclone", true) .. " Drop cyclone away from group."
	}
	mobs["vet_skyborn_tempest"] = {
		self:SpellDesc("Manifest Cyclone", true, 2) .. " Drop cyclone away from group."
	}
	mobs["aethros"] = {
		"Phase 1: Tank & spank. When adds come, kill them ASAP. 1 or 2 people can stun adds.",
		"Phase 2: Get teleported to entrance. Run back to boss, dodging tornados and telegraphs. Boss casts Tempest.",
		self:SpellDesc("Tempest", true, 2),
		"After interrupting, the fight will switch back to Phase 1. Continue these until the fight ends."
	}
	mobs["vet_aethros"] = {
		"Phase 1: Tank and spank, and avoid telegraphs.",
		"Phase 2: Let tank pick up Gust adds, then kill them ASAP. One person will get an AoE under them. Sprint to the side wall and drop it there.",
		"Phase 3: Get teleported to entrance. Run back to boss, dodging tornados and telegraphs. Boss casts Tempest.",
		self:SpellDesc("Tempest", true, 3),
		"After interrupting, the fight will loop back to Phase 1.",
		"During third Phase 1, tornadoes continue to move. Dodge them."
	}
	mobs["arcanist_breezebinder"] = {
		self:SpellDesc("Gust Convergence", true, 2),
		"Gust Convergence casts quickly and is VERY high priority - if not interrupted need to stun the adds.",
		self:SpellDesc("Manifest Cyclone", false) .. " Drop cyclones away from the group."
	}
	mobs["vet_arcanist_breezebinder"] = {
		self:SpellDesc("Gust Convergence", true, 2),
		self:SpellDesc("Manifest Cyclone", false) .. "Drop cyclones away from the group."
	}
	mobs["stormtalon"] = {
		"All but tank stack on one side of the boss (avoid cleave attacks)",
		"Static Charge puts a series of small telegraphs under a random person - run until they stop. Avoid dropping them on the group.",
		"Move toward center of the room when boss does; it means knockback and Static Wave are coming.", 
		"After knockback, CC break out of the stun, *strafe sideways back to the middle*, and interrupt the boss's Static Wave ASAP!!",
		"When someone is 'Eye of the Storm', stack on that player and avoid small telgraphs.",
		"Player controlling Eye of the Storm should move smoothly and slowly around the boss (no dashing). Try to stay close to boss so dps can continue."
	}
	mobs["vet_stormtalon"] = {
		"All but tank stack on one side of the boss (avoid cleave attacks)",
		"Static Charge puts a series of small telegraphs under every person - run until they stop. Avoid dropping them on the group.",
		"Move toward center of the room when boss does; it means knockback and Static Wave are coming.", 
		"After knockback, CC break out of the stun, *strafe sideways back to the middle*, and interrupt the boss's Static Wave ASAP!!",
		"When someone has 'Eye of the Storm', stay in the safe zone around that player and avoid small telgraphs.",
		"Eye of the Storm player will be disoriented. CC break or quickly find a strafe key.",
		"Move smoothly and slowly in a circle around the boss (no dashing!). Try to stay close to boss so dps can continue."	
	}
	
	--------------------------------------------------------------------------------------------------------------------------------
	-- Skullcano --
	mobs["grimgrim_blightmask"] = {
		self:SpellDesc("Blightbrush", true, 1, true) .. " - stuns and does damage."
	}
	mobs["vet_grimgrim_blightmask"] = {
		self:SpellDesc("Blightbrush", true, 1, true) .. " - stuns and does damage."
	}
	mobs["grimgrim_curseblade"] = {
		self:SpellDesc("Umbral Strike", true, 1) 
	}
	mobs["vet_grimgrim_curseblade"] = {
		self:SpellDesc("Umbral Strike", true, 1) 
	}
	mobs["ravenous_razorbeak"] = {
		self:SpellDesc("Ravenous Rush", true, 1) .. " high damage charge attack with knockdown",
		"Damage increased after Berserk"
	}
	mobs["vet_ravenous_razorbeak"] = {
		self:SpellDesc("Ravenous Rush", true, 1) .. " high damage charge attack with knockdown",
		"Damage increased after Berserk"
	}	
	mobs['grimgrim_hexweaver'] = {
		self:SpellDesc("Essence Drain", true, 1, true) .. " with enough dps, can out-damage the life drain"
	}	
	mobs['vet_grimgrim_hexweaver'] = {
		self:SpellDesc("Essence Drain", true, 1, true) .. " with enough dps, can out-damage the life drain"
	}
	mobs['grimgrim_doomcaller'] = {
		self:SpellDesc("Condemnation", true, 2) .. " - if not interrupted drop away from group"
	}	
	mobs['vet_grimgrim_doomcaller'] = {
		self:SpellDesc("Conjure Spirits", true, 2, true) .. " - if not interrupted kill adds",
		self:SpellDesc("Condemnation", true, 2) .. " - if not interrupted drop away from group"
	}	
	mobs['redmoon_jailer'] = {
		'Only kill jailer; prisoners will run away',
		self:SpellDesc("Enslave", true, 2) .. " -  if not interrupted kill tether ASAP!"
	}
	mobs['vet_redmoon_jailer'] = {
		'Only kill jailer; prisoners will run away',
		self:SpellDesc("Enslave", true, 2) .. " - if not interrupted kill tether ASAP!"
	}
	mobs['redmoon_gold_guardian'] = {
		self:SpellDesc('Marauder Cleave', true, 1)
	}

	mobs['vet_redmoon_gold_guardian'] = {
		self:SpellDesc('Marauder Cleave', true, 1)
	}
	mobs['redmoon_gold_hoarder'] = {
		self:SpellDesc('Debilitating Barrage', true)
	}
	mobs['vet_redmoon_gold_hoarder'] = {
		self:SpellDesc('Debilitating Barrage', true, 1)
	}
	mobs['redmoon_canoneer'] = {
		self:SpellDesc('Marauder Mortar', true, 1)
	}
	mobs['vet_redmoon_canoneer'] = {
		self:SpellDesc('Marauder Mortar', true, 1)
	}	
	mobs['redmoon_razortail'] = {
		self:SpellDesc('Clawing Frenzy', true)
	}	
	mobs['vet_redmoon_razortail'] = {
		self:SpellDesc('Clawing Frenzy', true)
	}
	mobs['redmoon_wrestler'] = {
		"Very high damage abilities!",
		self:SpellDesc("Fixation", true),
		self:SpellDesc("Trample", true),
		self:SpellDesc("Pile Driver", true)
	}
	mobs['vet_redmoon_wrestler'] = {
		"Very high damage abilities!",
		self:SpellDesc("Fixation", true),
		self:SpellDesc("Trample", true),
		self:SpellDesc("Pile Driver", true)
	}
	mobs['redmoon_crusher'] = {
		self:SpellDesc("Leap") .. " - disorients. Hard to avoid.",
		self:SpellDesc("Twinkle Toes", true, 2, true)
	}
	mobs['vet_redmoon_crusher'] = {
		self:SpellDesc("Leap") .. " - disorients. Hard to avoid.",
		self:SpellDesc("Twinkle Toes", true, 2, true)
	}
	mobs['redmoon_furyspitter'] = {
		self:SpellDesc("Gold swell", true, 1, true),
		"Also occasionally calls down a missile."
	}
	mobs['vet_redmoon_furyspitter'] = {
		self:SpellDesc("Gold swell", true, 1, true),
		"Also occasionally calls down a missile."
	}
	mobs['redmoon_necromancer'] = {
		self:SpellDesc('Vitae Siphon', true, 2) .. " - VERY high damage AoE attack!"
	}
	mobs['vet_redmoon_necromancer'] = {
		self:SpellDesc('Vitae Siphon', true, 2) .. " - VERY high damage AoE attack!"
	}
	mobs['redmoon_sniper'] = {
		self:SpellDesc('Dead Eye', true) .. " - VERY high damage!"
	}
	mobs['vet_redmoon_sniper'] = {
		self:SpellDesc('Dead Eye', true, 1) .. " - VERY high damage!"
	}
	mobs['stewshaman_tugga'] = {
		"Phase 1: ",
		self:SpellDesc("Molten Rain", true, 2),
		self:SpellDesc("Magma Totem", true, 2, true),
		self:SpellDesc("Into the Stew", true, 2) .. " if thrown in stew, use breakout mechanic to escape ASAP!",
		"Phase 2: Starts with short knockback/disarm. EVERYONE eat the meat Tugga throws out. If he is about to finish eating, can interrupt.",
		"After food is gone, rotates back to phase 1."
	}
	mobs['vet_stewshaman_tugga'] = {
		"Phase 1: ",
		self:SpellDesc("Molten Rain", true, 2),
		self:SpellDesc("Magma Totem", true, 2, true),
		self:SpellDesc("Into the Stew", true, 2) .. " if thrown in stew, use breakout mechanic to escape ASAP!",
		"Phase 2: Starts with short knockback/disarm. EVERYONE eat the meat Tugga throws out. If he is about to finish eating, can interrupt.",
		"After food is gone, rotates back to phase 1."
	}
	mobs['thunderfoot'] = {
		"No interrupts needed, movement ability helpful.",
		"Group behind boss (avoid frontal cleave).",
		"Jump every time he does Thunder Pound (cast twice in a row) or Pulverize (after Enraging Fury)",
		"Pulverize is a huge telegraph - sprint or use movement ability to get out and jump.",
		"Bring Thunderfoot to mushrooms when possible. He gets stunned when he breaks one."
	}
	mobs['vet_thunderfoot'] = {
		"No interrupts needed, movement ability helpful.",
		"Group behind boss (avoid frontal cleave).",
		"Jump every time he does Thunder Pound (cast twice in a row) or Pulverize (after Enraging Fury)",
		"Pulverize is a huge telegraph - sprint or use movement ability to get out and jump.",
		"Bring Thunderfoot to mushrooms when possible. He gets stunned when he breaks one.",
		"Lots of small damage pools to avoid."
	}
	mobs['bosun_octog'] = {
		"CC break recommended",
		"Phase 1:",
		"Hookshot: grips anyone in the telegraph to Octog. The targeted player will always be gripped in and stunned. Other players should avoid it.",
		self:SpellDesc("Shred", true, 2, true) .. " - always cast after Hookshot. Use CC breaker to get out before Shred starts.",
		"Razooki: Octog's monkey companion. Kill ASAP or he will fling deadly poo.",
		"Phase 2:",
		"Octog shields himself and spawns a bunch of fixating squirg adds the group must avoid/kill. They explode for VERY high damage if you get inside their telegraphs.",
		"Noxious Ink: The person dripping Noxious Ink needs to keep running until the debuff is gone to avoid damage. Drop the ink pools around the sides of the room."	,
		"Alternates between phases until defeated."
	}
	mobs['vet_bosun_octog'] = {
			"CC break recommended",
		"Phase 1:",
		"Hookshot: grips anyone in the telegraph to Octog. The targeted player will always be gripped in and stunned. Other players should avoid it.",
		self:SpellDesc("Shred", true, 2, true) .. " - always cast after Hookshot. Use CC breaker to get out before Shred starts.",
		"Razooki: Octog's monkey companion. Kill ASAP or he will fling deadly poo.",
		"Phase 2:",
		"Octog shields himself and spawns a bunch of fixating squirg adds the group must avoid/kill. They explode for VERY high damage if you get inside their telegraphs.",
		"Noxious Ink: The person dripping Noxious Ink needs to keep running until the debuff is gone to avoid damage. Drop the ink pools around the sides of the room."	,
		"Alternates between phases until defeated."
	}
	mobs['quartermaster_gruh'] = {
		'Dead Eye: VERY high damage attack - avoid!',
		self:SpellDesc('Kneecap', true, 3, true) .. " can CC break Kneecap to remove the slow effect."
	}	
	mobs['vet_quartermaster_gruh'] = {
		"CC break recommended",
		'Dead Eye: VERY high damage attack - avoid!',
		self:SpellDesc('Kneecap', true, 3, true) .. " can CC break Kneecap to remove the slow effect.",
		self:SpellDesc('Debilitating Barrage') .. " another slow. Hard to avoid; use CC break.",
		self:SpellDesc('Fatal Shot', true, 3) .. " - instant kill shot, preceded by a disarm (CC break!). Get weapon and interrupt ASAP!"
	}	
	mobs['mordechai_redmoon'] = {
		"Phase 1: (Lasers)",
		"Large laser beems rotate clockwise or counter-clockwise. Run the direction the lasers go. They insta-kill.",
		"Avoid lava pools. They damage and stun. Use CC break or breakout if caught.",
		"Phase 2: (Boss)",
		"Stack on one side of boss (avoid front and back linear attacks)",
		"Turn away from the middle of the room when you see 'The Terraformer is overcharging'.",
		self:SpellDesc("Turret Grenade", true, 2, true) .. " - if not interupted, avoid turret's energy bullets. They move in a line.",
		self:SpellDesc("Big Bang", true, 2, true) .. " - Stuns the target. Target can use CC break to get out of telegraph. Intsa-kill for anyone stuck in telegraph. Starts on 2nd phase 1.",
		self:SpellDesc("Vicious Barrage", true, 2) .. " - X-shaped aoe centered on boss. Each tick moves safe zones clockwise. Interrupt immediately! Starts on 3rd phase 1.",
		"After Phase 2, goes back to phase 1"
	}
	mobs['vet_mordechai_redmoon'] = {
		"Phase 1: (Lasers)",
		"Large laser beems rotate clockwise or counter-clockwise. Run the direction the lasers go. They insta-kill.",
		"Avoid lava pools. They damage and stun. Use CC break or breakout if caught.",
		"Small lasters move the opposite way of big lasers. Jump over them to avoid. Starts in Phase 2.",
		"Phase 2: (Boss)",
		"Stack on one side of boss (avoid front and back linear attacks)",
		"Turn away from the middle of the room when you see 'The Terraformer is overcharging'.",
		self:SpellDesc("Turret Grenade", true, 2, true) .. " - if not interupted, avoid turret's energy bullets. They move in a line.",
		self:SpellDesc("Big Bang", true, 2, true) .. " - Stuns the target. Target can use CC break to get out of telegraph. Intsa-kill for anyone stuck in telegraph. Starts on 2nd phase 1.",
		self:SpellDesc("Vicious Barrage", true, 2) .. " - X-shaped aoe centered on boss. Each tick moves safe zones clockwise. Interrupt immediately! Starts on 3rd phase 1.",
		"After Phase 2, goes back to phase 1"
	}

	--------------------------------------------------------------------------------------------------------------------------------
	-- Sanctuary of the Swordmaiden --
	
	mobs['zealous_clawsister'] = {
		"Stun as often as possible. Stack up so Predators don't pounce all over."
	}	
	mobs['vet_zealous_clawsister'] = {
		"Stun as often as possible. Stack up so Predators don't pounce all over."
	}	
	mobs['zealous_lifesinger'] = {
		"*INTERRUPT* Vitarra's Will (2IA) - creates a large life drain circle. If not interrupted, get out of circle ASAP."
	}	
	mobs['vet_zealous_lifesinger'] = {
		"*INTERRUPT* Vitarra's Will (2IA) - creates a large life drain circle. If not interrupted, get out of circle ASAP."
	}	
	mobs['zealous_battlemaiden'] = {
		"Avoid frontal Sonic Slash.",
		"Interrupt Lifesinger's Vitarra's Will."
	}		
	mobs['vet_zealous_battlemaiden'] = {
		"Avoid frontal Sonic Slash.",
		"Interrupt Lifesinger's Vitarra's Will."
	}	
	mobs['corrupted_veteran_swordmaiden'] = {
		"Very high damage abilities!",
		self:SpellDesc('Wicked Slash') .. " - frontal cleave, all but tank must avoid",
		self:SpellDesc('Whirlwind', true, 2)
	}
	mobs['vet_corrupted_veteran_swordmaiden'] = {
		"Very high damage abilities!",
		self:SpellDesc('Wicked Slash') .. " - frontal cleave, all but tank must avoid",
		self:SpellDesc('Whirlwind', true, 2)
	}
	mobs['moldwood_mauler'] = {
		self:SpellDesc("Mighty Leap", true, 2, true) .. " - if not interrupting, run to avoid.",
		self:SpellDesc("Foul Scourge", true, 2)
	}
	mobs['vet_moldwood_mauler'] = {
		self:SpellDesc("Mighty Leap", true, 2, true) .. " - if not interrupting, run to avoid.",
		self:SpellDesc("Foul Scourge", true, 2)
	}
	mobs['corrupted_deathsting_hive_defender'] = {
		self:SpellDesc("Honey Snare"),
		"Stun the bugs as much as possible to avoid their annoying charge attacks."
	}
	mobs['vet_corrupted_deathsting_hive_defender'] = {
		self:SpellDesc("Honey Snare"),
		"Stun the bugs as much as possible to avoid their annoying charge attacks."
	}
	mobs['elder_priestess'] = {
		self:SpellDesc("Well of Life", true, 2) .. " - if not interrupted, targeted player must move out of group.",
		"Casts a large blind, with small damage. Can be avoided, cc broken, or healed through."
	}
	mobs['vet_elder_priestess'] = {
		self:SpellDesc("Well of Life", true, 2) .. " - if not interrupted, targeted player must move out of group.",
		"Casts a large blind, with small damage. Can be avoided, cc broken, or healed through."
	}
	mobs['lifeweaver_soulbinder'] = {
		self:SpellDesc("Invocation", true, 2)
	}
	mobs['vet_lifeweaver_soulbinder'] = {
		self:SpellDesc("Invocation", true, 2)
	}
	mobs['lifeweaver_initiate'] = {
		self:SpellDesc("Titan's Fist", true, 0, true),
		"Interrupt Soulbinder's Invocation."
	}
	mobs['vet_lifeweaver_initiate'] = {
		self:SpellDesc("Titan's Fist", true, 0, true),
		"Interrupt Soulbinder's Invocation."
	}
	mobs['corrupted_terrorantula'] = {
		self:SpellDesc("Toxic Web"),
		self:SpellDesc("Burst"),
		"Chain stun small spider adds."
	}
	mobs['vet_corrupted_terrorantula'] = {
		self:SpellDesc("Toxic Web"),
		self:SpellDesc("Burst"),
		"Chain stun small spider adds."
	}
	mobs['blighted_moldwood_asura'] = {
		"Chain stun these groups. They teleport around annoyingly, but their attacks don't do too much damage."
	}
	mobs['vet_blighted_moldwood_asura'] = {
		"Chain stun these groups. They teleport around annoyingly, but their attacks don't do too much damage."
	}
	mobs['moldwood_skurge_tactician'] = {
		self:SpellDesc("Reinforce", true, 2),
		self:SpellDesc("Rupture", true, 2, true)
	}
	mobs['vet_moldwood_skurge_tactician'] = {
		self:SpellDesc("Reinforce", true, 2),
		self:SpellDesc("Rupture", true, 2, true)
	}
	mobs['moldwood_skurge_slasher'] = {
		self:SpellDesc("Crippling Slash", true, 2) .. " - very annoying disarm. If not interrupted, move continuously during the cast to avoid.",
		self:SpellDesc("Sundering Surge", true, 2, true)
	}
	mobs['vet_moldwood_skurge_slasher'] = {
		self:SpellDesc("Crippling Slash", true, 2) .. " - very annoying disarm. If not interrupted, move continuously during the cast to avoid.",
		self:SpellDesc("Sundering Surge", true, 2, true)
	}
	mobs['deadringer_shallaos'] = {
		"Aggro is based on Resonance stacks from getting hit or hitting Torine Chimes. Tank keep 1 stack higher than others. DPS/heal avoid hitting chimes.",
		"Tank will hit a chime to start fight.",
		"Phase 1: ",
		"Tank Shallaos at the wall where she started, so that she faces the wall. DPS and heal stay behind boss to avoid frontal cleave.",
		self:SpellDesc("Echo", true, 3),
		"Phase 2: Dodge Chime pulses and the Echoes that come from the corners of the room. Remember to call out if your stacks get high so tank can compensate.",
		"Prepare to interrupt Echo as soon as phase 1 starts",
		"Rotates back to phase 1."
	}
	mobs['vet_deadringer_shallaos'] = {
		"Aggro is based on Resonance stacks from getting hit or hitting Torine Chimes. Tank must keep 1 stack higher than others. DPS/heal avoid hitting chimes.",
		"Tank will hit a chime to start fight.",
		"Phase 1: ",
		"Tank Shallaos at the wall where she started, so that she faces the wall. DPS and heal stay behind boss to avoid frontal cleave.",
		self:SpellDesc("Echo", true, 3),
		"Phase 2: Dodge Chime pulses and the Echoes that come from the corners of the room. Remember to call out if your stacks get high so tank can compensate.",
		"Prepare to interrupt Echo as soon as phase 1 starts",
		"Rotates back to phase 1.",
		"Final phase 1 (under 25% hp) combines Echos from the corners of the room, Chime pulses, and all of Shallaos's attacks. Burn her."
	}
	mobs['rayna_darkspeaker'] = {
		"Nothing to interrupt. CC break and movement skill recommended.",
		"Phase 1: ",
		"Drop lava pools away from the group.",
		self:SpellDesc("Raging Lava"),
		"CC break if Flame Leash is on you",
		"Phase 2: ",
		"Avoid Molten Waves that spawn from the back of the room.",
		"Alternates between phases 1 and 2."
	}
	mobs['vet_rayna_darkspeaker'] = {
		"Nothing to interrupt. CC break and movement skill recommended.",
		"Phase 1: ",
		"Drop lava pools away from the group, then stack back up for healing.",
		self:SpellDesc("Raging Lava"),
		"CC break if Flame Leash is on you",
		"Phase 2: ",
		"Avoid Molten Waves that spawn from the back of the room.",
		"Alternates between phases 1 and 2.",
		"In 3rd Phase 1, Molten Waves spawn from any direction. Flame leash is no longer cast."
	}

	mobs['ondu_lifeweaver'] = {
		"Avoid or interrupt Plague Smash.",
		"Party will be pulled to Ondu and rooted, except for one person.",
		"That person must walk over all of the Blight Bombs before they reach the party."
		}

	mobs['vet_ondu_lifeweaver'] = {
		"Avoid or interrupt Plague Smash.",
		"Party will be pulled to Ondu and rooted, except for one person.",
		"That person must walk over all of the Blight Bombs before they reach the party.",
		self:SpellDesc("Life Weave", true, 2),
		"Tank must kite the boss slowly for sheild phase, full speed when Ondo enrages."
	}

	mobs['moldwood_overlord_skash'] = {
		"Tank, spank, avoid telegraphs.",
		"Interrupt whenever possible."
	}

	mobs['vet_moldwood_overlord_skash'] = {
		"Tank in center of room. Move to side when eggs are about to spawn.",
		"Run back to center after eggs spawn, interrupting eggs on the way.",
		"Spread out in a small circle around hammer when Hammer Pulse is coming. Stun adds if needed.",
		"Ability order is Summon Swarm, Tentacle Wrath, Hammer Pulse, Summon Swarm, Tentacle Wrath, Corruption Siphon (eggs)."
	}

	mobs['spiritmother_selene_the_corrupted'] = {
		"CC break recommended. Interrupts required.",
		"Mostly tank and spank. Interrupt Nightmare Ripple (3 IA) whenever possible.",
		"Always stand in the light if it's dark. Stay together.",
		"Person with purple over head move to the side.",
		"After dark, group near water for heals. Get blinded (CC break). Interrupt and kill adds. AVOID SHADOW WAVES.",
		"When tethers start, CC break or kill tether.",
		"For Blackout, Get in the light!"
	}

	mobs['vet_spiritmother_selene_the_corrupted'] = {
		"CC break recommended. Interrupts required.",
		"Mostly tank and spank. Interrupt Nightmare Ripple (3 IA) whenever possible.",
		"Always stand in the light if it's dark. Stay together.",
		"Person with purple over head move to the side.",
		"After dark, group near water for heals. Get blinded (CC break). Interrupt and kill adds. AVOID SHADOW WAVES.",
		"When tethers start, CC break or kill tether.",
		"For Blackout, Get in the light!"
	}

	mobs['corrupted_lifecaller_khalee'] = {
		"Keep Calm for blind.",
		self:SpellDesc("Well of Life", true, 2)
	}

	mobs['vet_corrupted_lifecaller_khalee'] = {
		"Keep Calm for blind.",
		self:SpellDesc("Well of Life", true, 2),
		"Interrupt add when it casts Desecrating Eruptions (2 IA)"
	}

	mobs['corrupted_edgesmith_torian'] = {
		"Wide frontal cleave with dot - stay behind!",
		"Avoid Crippling Slash by running for the whole cast",
		"When she moves to the center Blade Dance is coming.",
		"Interrupt Blade Dance (4 IA) or move back and dodge."
	}

	mobs['vet_corrupted_edgesmith_torian'] = {
		"Wide frontal cleave with dot - stay behind!",
		"Avoid Crippling Slash by running for the whole cast",
		"When she moves to the center Blade Dance is coming.",
		"Interrupt Blade Dance (4 IA) or move back and dodge."
	}
	
	mobs['corrupted_deathbringer_dareia'] = {
		"High damage cleave - stay behind!",
		self:SpellDesc("Whirlwind", true, 2),
		self:SpellDesc("Warrior's Charge", true, 2, true),
		"Avoid red circles left by Warrior's Charge."
	}

	mobs['vet_corrupted_deathbringer_dareia'] = {
		"High damage cleave - stay behind!",
		self:SpellDesc("Whirlwind", true, 2),
		self:SpellDesc("Warrior's Charge", true, 2, true),
		"Avoid red circles left by Warrior's Charge."
	}


	mobs['hammerfist_moldjaw'] = {
		self:SpellDesc("Mesmerize", true, 2),
		self:SpellDesc("Foul Scourge", true, 2),
		self:SpellDesc("Mighty Leap", true, 2, true)
	}

	mobs['vet_hammerfist_moldjaw'] = {
		self:SpellDesc("Mesmerize", true, 2),
		self:SpellDesc("Foul Scourge", true, 2),
		self:SpellDesc("Mighty Leap", true, 2, true)

	}

	mobs['lifeweaver_guardian'] = {
		self:SpellDesc("Taste of Death", true, 2),
		self:SpellDesc("Nature's Fury", true, 2, true)
	}

	mobs['vet_lifeweaver_guardian'] = {
		self:SpellDesc("Taste of Death", true, 2),
		self:SpellDesc("Nature's Fury", true, 2, true),
		"Tank should CC break if snare stacks too high.",
		"Tank will become unhealable. Save a cooldown."
	}
	
	mobs['flamecrazed_demon'] = {
		"No interrupts required. Pure DPS/survival check.",
		"Small adds explode when touched. Trigger explosion then move away fast.",
		"Avoid rain of fire.",
		"Fire damage increases as fight continues."
	}

	mobs['vet_flamecrazed_demon'] = {
		"No interrupts required. Pure DPS/survival check.",
		"Small adds explode when touched. Trigger explosion then move away fast.",
		"Avoid rain of fire.",
		"Fire damage increases as fight continues."
	}

	
		
	
	diCrashCourses = mobs
end

-----------------------------------------------------------------------------------------------
-- CrashCourseExtended Instance
-----------------------------------------------------------------------------------------------
local CrashCourseExtendedInst = CrashCourseExtended:new()
CrashCourseExtendedInst:Init()
